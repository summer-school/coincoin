package fr.univpau.coincoin;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<TextView> textViews;
    private final int NUMBER_ELEMENTS = 4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //permet de bypasser le NetworkOnMainThreadException pour cet exercice
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy gfgPolicy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(gfgPolicy);
        }


        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViews = new ArrayList<TextView>(NUMBER_ELEMENTS);
        textViews.add(findViewById(R.id.textView1));
        textViews.add(findViewById(R.id.textView2));
        textViews.add(findViewById(R.id.textView3));
        textViews.add(findViewById(R.id.textView4));

        new AsyncTaskCoinAPI().execute("https://api.alternative.me/v1/ticker/?limit="+NUMBER_ELEMENTS);
    }

    private class AsyncTaskCoinAPI extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... urls) {
            try {
                return getJSONData(urls[0]);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                try {
                    parseJSONData(result);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.getStackTraceString(e);
                }
            } else {
                Toast toast = Toast.makeText(MainActivity.this, "Erreur de récupération des données", Toast.LENGTH_LONG);
                toast.show();
            }
        }

        private String getJSONData(String url) throws IOException {
            URL urlObject = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlObject.openConnection();
            connection.setRequestMethod("GET");
            connection.connect();

            InputStream inputStream = connection.getInputStream();

            String result = Helpers.InputStreamToString(inputStream);

            connection.disconnect();

            return result;
        }

        private void parseJSONData(String data) throws JSONException {
            JSONArray jsonArray = new JSONArray(data);
            for (int i = 0; i < NUMBER_ELEMENTS; i++) {
                String priceUSDasString = jsonArray.getJSONObject(i).getString("price_usd");
                double priceUSDasDouble = Double.parseDouble(priceUSDasString);

                //convertion en euros
                URL urlObject = null;
                try {
                    urlObject = new URL("https://public.opendatasoft.com/api/explore/v2.1/catalog/datasets/euro-exchange-rates/records?limit=1&refine=date:%222024%22&refine=currency:%22USD%22");
                    HttpURLConnection connection = (HttpURLConnection) urlObject.openConnection();
                    connection.setRequestMethod("GET");
                    connection.connect();

                    InputStream inputStream = connection.getInputStream();
                    String rateData = Helpers.InputStreamToString(inputStream);

                    JSONObject jsonObject = new JSONObject(rateData);
                    double freshRate = jsonObject.getJSONArray("results").getJSONObject(0).getDouble("rate");

                    Double priceEURasDouble = priceUSDasDouble / freshRate;
                    String priceEURasString = String.format("%.2f", priceEURasDouble);
                    textViews.get(i).append(priceEURasString + "€\n");

                    connection.disconnect();

                } catch (IOException e) {
                    e.printStackTrace();
                    Log.getStackTraceString(e);
                }






            }
        }
    }
}
